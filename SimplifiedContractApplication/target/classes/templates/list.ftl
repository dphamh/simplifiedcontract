<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Customer </span></div>
		<div class="panel-body">
	        <div class="formcontainer">
	            <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
	            <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
	            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.user.id" />
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="customerId">Customer Id</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.customerId" id="x" class="customerId form-control input-sm" placeholder="Enter Customer Id" required ng-minlength="1"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">
	                        <button type="button" ng-click="ctrl.getAllCustomerContracts()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Search Contract</button>
	                    </div>
	                </div>
	            </form>
    	    </div>
		</div>	
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Contracts </span></div>
		<div class="panel-body">
			<div class="table-responsive">
		        <table class="table table-hover">
		            <thead>
		            <tr>
		                <th>ID</th>
		                <th>CUSTOMER NAME</th>
		                <th>PRODUCT</th>
		                <th>PRICE</th>
		                <th>PURCHASED DATE</th>
		                <th>WARRANTY DURATION</th>
		                <th>UNIT</th>
		                <th width="100"></th>
		            </tr>
		            </thead>
		            <tbody>
		            <tr ng-repeat="c in ctrl.getAllContracts()">
		                <td>{{c.id}}</td>
		                <td>{{c.fullname}}</td>
		                <td>{{c.product}}</td>
		                <td>{{c.price}}</td>
		                <td>{{c.createdDate}}</td>
		                <td>{{c.warrantyDuration}}</td>
		                <td>{{c.warrantyUnit}}</td>
		                <td><button type="button" ng-click="ctrl.viewContract(c.id)" class="btn btn-success custom-width"> Download </button></td>
		            </tr>
		            </tbody>
		        </table>		
			</div>
		</div>
    </div>
</div>