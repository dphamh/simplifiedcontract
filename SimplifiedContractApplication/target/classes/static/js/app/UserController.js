'use strict';

angular.module('crudApp').controller('UserController',
    ['UserService', '$scope',  function( UserService, $scope) {

        var self = this;
        self.contract = {};
        self.submit = submit;
        self.getAllCustomerContracts = getAllCustomerContracts;
        self.viewContract = viewContract;
        self.getAllContracts = getAllContracts;
        
        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

    	 
        function submit(){
        	
        }
        function viewContract(id){
            console.log('View customer contract '+ id);
            UserService.viewContract(id)
                .then(
                    function(){
                        console.log('Contract is retreive successfully');
                    },
                    function(errResponse){
                        console.error('Error while retrieve contract '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllCustomerContracts(){
	    if (this.customerId !== undefined){
		 return UserService.getAllCustomerContracts(this.customerId);
	    } else {
		return;
	    }
        }
        
        function getAllContracts(){
        	return UserService.getAllContracts();
        }
    }


    ]);
