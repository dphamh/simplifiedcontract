'use strict';

angular.module('crudApp').factory('UserService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
        		getAllCustomerContracts: getAllCustomerContracts,
        		getAllContracts: getAllContracts,
        		viewContract: viewContract
            };

            return factory;

            function getAllCustomerContracts(customerId) {
		if (customerId === undefined) return;
                console.log('Fetching all customer contracts');
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API + "/list/" + customerId)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all customer contracts');
                            $localStorage.contracts = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading customer contracts');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllContracts(){
                return $localStorage.contracts;
            }

            function viewContract(id) {
                console.log('Fetching contract with id :'+id);
                var deferred = $q.defer();
                window.open(urls.USER_SERVICE_API + '/get/' + id);
                return deferred.promise;
            }
        }
    ]);
