# Simplified Contract Application
1 - Technology stack : 
  Spring Boot v2 : Quickly development and independent environment, executable JAR
  AngularJs: Client side web app for lightweight and not depend on API module
  DB : use different DBMS for different purpose by declare different profile in    application yml 
  iText for generating PDF

2 - Limitation:

  + All in once JAR file for speed up and simplify demo purpose
  + Use H2 in memory DB for not require any additional set up
  + No authenticate and security implement at this stage, just for POC purpose
  + Low availability and scalability support

3 - Possible Enhance

  + Implement as Component Design "SimplifiedContract_ComponentDesign" attached to remove constrains between component.
  + Support any RBDMS or NoSQL DB for future usage when data grow
  + Apply Spring security or OAuth2 mechanism for authenticate the communication between component
  + Support many type of report for better vision from management view
  + Deploy on cloud environment for using the ability and feature from that : scalability, availability, security, stable and fasten deploy and release new version

4 - How to use:

  Step 1: Run application by one of two way
  
  1 - Run from pre-built demo app
  Run : java -jar -Dspring.profiles.active=local ./target/SimplifiedContract-1.0.0.jar
  
  2 - Build from source
     + Move to source folder by : cd SimplifiedContractApplication
     + Make maven build : mvn clean install -Dmaven.test.skip=true
	 + Run : java -jar -Dspring.profiles.active=local ./target/SimplifiedContract-1.0.0.jar

  Step 2: Test API and Generate contract using Postman

  + Open Postman and use following request for testing
  --> Generate Contract
      * URL  : localhost:8080/sc/api/contract/generate
      * Type : POST
      * Body : raw
               content-type : application/xml

               <contract>
			<customerId>1</customerId>
			<price>10000000</price>
			<fullname>Michael Owen</fullname>
			<shortname>Owen</shortname>
			<product>4Gb Monthly Package</product>
			<warrantyUnit>DAY</warrantyUnit>
			<warrantyDuration>365</warrantyDuration>	
		</contract>
      * Expected Response : Contract id has just generated

   --> List Contract by CustomerID (In previous step is 1)
      * URL  : localhost:8080/sc/api/contract/list/1    
      * Type : GET
      * Expected Response : List of contract of this customer in JSON format

  --> Show Contract by contract Id (return by Generate Contract API) - NOT USING POSTMAN and test directly with browser
      * Paste the URL into browser : localhost:8080/sc/api/contract/get/{contractId}
        Ex : localhost:8080/sc/api/contract/get/1
      * Result : PDF contract show and can be download

  Step 3: Test Customer Web App
  
  --> Open web browser with this URL : localhost:8080/sc
  --> Fill in customer Id and click "Search Contract" button to query all contract of this customer (Ex : CustomerId = 1)
  --> Base on the action click on "Search Contract" button, list of contract will be shown on table "List Of Contract"
  --> Click on "Download" button to show the contract and download it to local storage if they want.