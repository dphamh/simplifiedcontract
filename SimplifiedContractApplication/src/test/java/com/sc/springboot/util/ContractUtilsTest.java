package com.sc.springboot.util;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.sc.springboot.model.Contract;

class ContractUtilsTest
{
    static final Contract contract = ScTestUtils.createSampleContract();
    static final ByteArrayInputStream bis = ContractUtils.buildPdfContract(contract);
    @BeforeAll
    static void setUpBeforeClass() throws Exception
    {
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
    }

    @BeforeEach
    void setUp() throws Exception
    {
    }

    @AfterEach
    void tearDown() throws Exception
    {
    }

    @Test
    void testBuildPdfContractSuccess()
    {
        try {
            String pdfStr = ScTestUtils.extractPdfText(bis);
            assertTrue( pdfStr.contains(contract.getFullname()));
            assertTrue( pdfStr.contains(contract.getProduct()));
            assertTrue( pdfStr.contains(String.valueOf(contract.getPrice())));
            assertTrue( pdfStr.contains(contract.getWarrantyDuration().toString()));
            assertTrue( pdfStr.contains(contract.getWarrantyUnit().toString()));
        }
        catch (IOException e) {
            fail("can not access pdf file");
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
