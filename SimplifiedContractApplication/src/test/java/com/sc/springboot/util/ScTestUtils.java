package com.sc.springboot.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import com.sc.springboot.model.Contract;
import com.sc.springboot.model.Contract.WARRANTYUNIT;

public class ScTestUtils
{
    public static Contract createSampleContract() {
        long customerId = 11;
        String fullname = "fullname";
        String shortname = "shortname";
        String product = "product";
        double price = 100;
        WARRANTYUNIT unit = WARRANTYUNIT.DAY;
        Double warrantyDuration = new Double(365);
        
        return new Contract(customerId, shortname, fullname, product, price, unit, warrantyDuration);
    }
    
    public static String extractPdfText(ByteArrayInputStream bis) throws IOException {
        PDDocument pdfDocument = PDDocument.load(bis);
        try {
           return new PDFTextStripper().getText(pdfDocument);
        } finally {
           pdfDocument.close();
        }
     }
}
