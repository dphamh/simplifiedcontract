package com.sc.springboot.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.sc.springboot.model.Contract.WARRANTYUNIT;
import com.sc.springboot.util.ScTestUtils;

class ContractModelTest
{

    @BeforeAll
    static void setUpBeforeClass() throws Exception
    {
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception
    {
    }

    @BeforeEach
    void setUp() throws Exception
    {
    }

    @AfterEach
    void tearDown() throws Exception
    {
    }

    @Test
    void testCreateContractModel()
    {
        long customerId = 11;
        String fullname = "fullname";
        String shortname = "shortname";
        String product = "product";
        double price = 100;
        WARRANTYUNIT unit = WARRANTYUNIT.DAY;
        Double warrantyDuration = new Double(365);
        
        Contract c = ScTestUtils.createSampleContract();
        
        
        assertEquals(customerId, c.getCustomerId());
        assertEquals(fullname, c.getFullname());
        assertEquals(shortname, c.getShortname());
        assertEquals(product, c.getProduct());
        assertEquals(price, c.getPrice());
        assertEquals(unit, c.getWarrantyUnit());
        assertEquals(warrantyDuration, c.getWarrantyDuration());
        assertTrue(Calendar.getInstance().getTime().getTime() - c.getCreatedDate().getTime() < 100);
        
    }

}
