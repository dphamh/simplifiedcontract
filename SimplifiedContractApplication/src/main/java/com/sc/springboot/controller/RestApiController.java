package com.sc.springboot.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sc.springboot.model.Contract;
import com.sc.springboot.service.IContractService;
import com.sc.springboot.util.ContractUtils;
import com.sc.springboot.util.ContractUtils.VALIDATE_STATUS;


@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
    IContractService contractService;

    @PostMapping(value = "/contract/generate",
    consumes = {MediaType.APPLICATION_XML_VALUE})
    public long generateContract(@RequestBody Contract contract) {
        if (ContractUtils.validateContract(contract) != VALIDATE_STATUS.PASSED) {
            ResponseEntity
            .badRequest();
        }
        if (contract.getCreatedDate() == null) {
            contract.setCreatedDate(Calendar.getInstance().getTime());
        }
        return contractService.saveContract(contract);
    }
    
    @GetMapping(value = "/contract/list/{customerId}", 
    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Contract> listContract(@PathVariable long customerId) {
        return contractService.findAllByCustomerId(customerId);
    }
    
    
    @GetMapping(value = "/contract/get/{id}",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getContract(@PathVariable String id) throws IOException {
        
        Contract contract = contractService.getContractById(Long.valueOf(id).longValue());
        if (contract == null) {
            ResponseEntity
            .notFound();
        }
        
        ByteArrayInputStream bis = ContractUtils.buildPdfContract(contract);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("filename=%s_Contract.pdf", StringUtils.isEmpty(contract.getFullname()) ? contract.getShortname() : contract.getFullname()));
        
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}