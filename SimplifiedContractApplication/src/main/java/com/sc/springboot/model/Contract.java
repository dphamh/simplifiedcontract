package com.sc.springboot.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Contract")
@XmlRootElement(name = "contract")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contract
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    
    @XmlElement
    private long customerId;
    @XmlElement
    private String shortname;
    @XmlElement
    private String fullname;
    @XmlElement
    private String product;
    @XmlElement
    private double price;
    @XmlElement
    private WARRANTYUNIT warrantyUnit;
    @XmlElement
    private Double warrantyDuration;
    
    private Date createdDate;
    
    public enum WARRANTYUNIT {
        DAY, MONTH, YEAR;
    }
    
    public Contract() {
        
    }
    
    public Contract(long customerId, String shortname, String fullname, String product, double price, WARRANTYUNIT warrantyUnit, Double warrantyDuration) {
        this.customerId = customerId;
        this.shortname = shortname;
        this.fullname = fullname;
        this.product = product;
        this.price = price;
        this.warrantyUnit = warrantyUnit;
        this.warrantyDuration = warrantyDuration;
        this.createdDate = Calendar.getInstance().getTime();
    }
    
    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }
    
    public long getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(long customerId)
    {
        this.customerId = customerId;
    }

    public void setShortname(String shortname)
    {
        this.shortname = shortname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public void setWarrantyUnit(WARRANTYUNIT warrantyUnit)
    {
        this.warrantyUnit = warrantyUnit;
    }

    public void setWarrantyDuration(Double warrantyDuration)
    {
        this.warrantyDuration = warrantyDuration;
    }
    
    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
    
    public String getShortname()
    {
        return shortname;
    }

    public String getFullname()
    {
        return fullname;
    }

    public String getProduct()
    {
        return product;
    }

    public double getPrice()
    {
        return price;
    }

    public WARRANTYUNIT getWarrantyUnit()
    {
        return warrantyUnit;
    }

    public Double getWarrantyDuration()
    {
        return warrantyDuration;
    }
}
