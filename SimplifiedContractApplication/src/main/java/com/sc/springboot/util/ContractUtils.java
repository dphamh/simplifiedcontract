package com.sc.springboot.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sc.springboot.model.Contract;


public class ContractUtils
{
    public enum VALIDATE_STATUS {
        PASSED, FAILED;
    }
    public static VALIDATE_STATUS validateContract(Contract contract) {
        //TODO: validate all required contract field
        return VALIDATE_STATUS.PASSED;
        
    }
    
    public static ByteArrayInputStream buildPdfContract(Contract contract) {
        DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
     
        Document document = new Document();
        document.addAuthor("FSoft");
        document.addCreator("Simplified Contract");
        document.addKeywords("Eclectronic Contract");
        document.addTitle(String.format("Eclectronic Contract for customer : %s", contract.getFullname()));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        
        try {

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setWidths(new int[]{10, 10, 3, 10, 3});
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Customer Name", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Product", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Price", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Warranty Duration", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Unit", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);


            PdfPCell cell;

            cell = new PdfPCell(new Phrase(contract.getFullname()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(contract.getProduct()));
            cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(contract.getPrice())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPaddingRight(5);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase(String.valueOf(contract.getWarrantyDuration())));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPaddingRight(5);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase(contract.getWarrantyUnit().toString()));
            cell.setPaddingLeft(5);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(new Paragraph(String.format("Dear valued customer : %s !", contract.getFullname())));
            document.add( Chunk.NEWLINE );
            document.add(new Paragraph(String.format("This is an electronic contract for the product (%s) you have purchased in our store at %s. Please check the detail as below : ", contract.getProduct(), df.format(contract.getCreatedDate()))));
            document.add( Chunk.NEWLINE );
            document.add( Chunk.NEWLINE );
            
            document.add(table);
            
            document.close();
            
        } catch (DocumentException ex) {
        
            Logger.getLogger(ContractUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
