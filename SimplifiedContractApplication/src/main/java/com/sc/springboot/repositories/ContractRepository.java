package com.sc.springboot.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sc.springboot.model.Contract;


@Repository
public interface ContractRepository extends CrudRepository<Contract, Long> {

    @Query("Select c from Contract c where c.customerId = :customerId")
    List<Contract> findAllByCustomerId(@Param("customerId") long customerId);
}