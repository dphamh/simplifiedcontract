package com.sc.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.sc.springboot.configuration.JpaConfiguration;


@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.sc.springboot"})
public class SimplifiedContractApp {

	public static void main(String[] args) {
		SpringApplication.run(SimplifiedContractApp.class, args);
	}
}
