package com.sc.springboot.service;

import java.util.List;

import com.sc.springboot.model.Contract;


public interface IContractService
{
    public long saveContract(Contract contract);
    public List<Contract> findAll();
    public List<Contract> findAllByCustomerId(long customerId);
    public Contract getContractById(long id);
}
