package com.sc.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc.springboot.model.Contract;
import com.sc.springboot.repositories.ContractRepository;

@Service
public class ContractService implements IContractService
{

    @Autowired
    private ContractRepository repository;
    
    @Override
    public long saveContract(Contract contract) {
        Contract savedContract = repository.save(contract);
        return savedContract == null ? -1 : savedContract.getId();
    }
    
    @Override
    public List<Contract> findAll()
    {
        List<Contract> contracts = (List<Contract>) repository.findAll();
        return contracts;
    }

    @Override
    public List<Contract> findAllByCustomerId(long customerId)
    {
        List<Contract> contracts = (List<Contract>) repository.findAllByCustomerId(customerId);
        return contracts;
    }
    @Override
    public Contract getContractById(long id)
    {
        return repository.findOne(id);
    }

}
